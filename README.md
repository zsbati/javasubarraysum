We define the following:

A subarray of an n-element array is an array composed from a contiguous block of the original array's elements. For example, if [1,2,3], then the subarrays are ,[1] ,[1,2], [2,3], [1,2,3], [2] and [3]. Something like [1,3] would not be a subarray as it's not a contiguous subsection of the original array.
The sum of an array is the total sum of its elements.
An array's sum is negative if the total sum of its elements is negative.
An array's sum is positive if the total sum of its elements is positive.
Given an array of  integers, find and print its number of negative subarrays on a new line.

Input Format

The first line contains a single integer, n, denoting the length of array .
The second line contains n space-separated integers describing each element of the array.
